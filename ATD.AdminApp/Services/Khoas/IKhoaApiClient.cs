﻿using ATD.Application.Dtos;
using ATD.ViewModels.Catalog.Khoas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ATD.AdminApp.Services.Khoas
{
    public interface IKhoaApiClient
    {
        Task<PagedResult<KhoaViewModel>> GetKhoasPagings(GetKhoaPagingRequest request);
    }
}
