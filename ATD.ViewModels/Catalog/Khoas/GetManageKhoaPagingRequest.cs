﻿using ATD.ViewModels.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace ATD.ViewModels.Catalog.Khoas
{
    public class GetManageKhoaPagingRequest : PagingRequestBase
    {
        public string keyword { get; set; }
        public List<int> KhoaIds { get; set; }
    }
}
