﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ATD.ViewModels.Catalog.Khoas
{
    public class KhoaViewModel
    {
        public int Id { get; set; }
        public string TenKhoa { get; set; }
        public string Sdt { get; set; }
        public string Email { get; set; }
        public string MoTa { get; set; }
    }
}
