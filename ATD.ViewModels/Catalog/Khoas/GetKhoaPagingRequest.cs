﻿using ATD.ViewModels.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace ATD.ViewModels.Catalog.Khoas
{
    public class GetKhoaPagingRequest:PagingRequestBase
    {
        public int? KhoaId { get; set; }
    }
}
