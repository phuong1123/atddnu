﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ATD.ViewModels.Catalog.NamHocs
{
    public class NamHocUpdateRequest
    {
        public int Id { get; set; }
        public string TenNamHoc { get; set; }
        public string GhiChu { get; set; }
    }
}
