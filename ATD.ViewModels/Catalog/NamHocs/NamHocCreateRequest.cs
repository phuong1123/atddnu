﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ATD.ViewModels.Catalog.NamHocs
{
    public class NamHocCreateRequest
    {
        public string TenNamHoc { get; set; }
        public string GhiChu { get; set; }
    }
}
