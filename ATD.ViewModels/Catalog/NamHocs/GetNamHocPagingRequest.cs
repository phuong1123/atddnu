﻿using ATD.ViewModels.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace ATD.ViewModels.Catalog.NamHocs
{
    public class GetNamHocPagingRequest:PagingRequestBase
    {
        public int? NamHocId { get; set; }
    }
}
