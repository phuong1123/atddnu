﻿using ATD.ViewModels.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace ATD.ViewModels.Catalog.NamHocs
{
    public class GetManageNamHocPagingRequest : PagingRequestBase
    {
        //public int? NamHocId { get; set; }

        public string keyword { get; set; }
        public List<int> NamHocIds { get; set; }
    }
}
