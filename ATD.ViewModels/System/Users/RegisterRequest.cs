﻿using ATD.Data.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace ATD.ViewModels.System.Users
{
    public class RegisterRequest
    {
        public string MaNhanVien { get; set; }
        public string HoDem { get; set; }
        public string Ten { get; set; }
        public int KhoaId { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public DateTime NgaySinh { get; set; }
        public string TrinhDo { get; set; }
        public string ChuyenMonGiangDay { get; set; }
        public LoaiGiangVien LoaiGiangVien { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
