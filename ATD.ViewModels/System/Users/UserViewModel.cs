﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ATD.ViewModels.System.Users
{
    public class UserViewModel
    {
        public Guid Id { get; set; }
        public string HoDem { get; set; }
        public string Ten { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
    }
}
