﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace ATD.ViewModels.System.Users
{
    public class LoginRequestValidator:AbstractValidator<LoginRequest>
    {
        public LoginRequestValidator()
        {
            RuleFor(x => x.Username).NotEmpty().WithMessage("Tài khảon là bắt buộc");
            RuleFor(x => x.Password).NotEmpty().WithMessage("Mật khẩu là bắt buộc")
                .MinimumLength(6).WithMessage("Mật khẩu tối thiểu 6 ký tự");
        }
    }
}
