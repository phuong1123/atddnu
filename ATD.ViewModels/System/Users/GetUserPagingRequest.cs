﻿using ATD.ViewModels.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace ATD.ViewModels.System.Users
{
    public class GetUserPagingRequest:PagingRequestBase
    {
        public string keyword { get; set; }
    }
}
