﻿using System;
using System.Collections.Generic;
using System.Text;
using ATD.Data.Entities;
using ATD.Data.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ATD.Data.Configurations
{
    public class ChiTietDiemDanhConfiguration : IEntityTypeConfiguration<ChiTietDiemDanh>
    {
        public void Configure(EntityTypeBuilder<ChiTietDiemDanh> builder)
        {
            builder.ToTable("ChiTietDiemDanhs");
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).UseIdentityColumn();
            builder.Property(x => x.LyDo).HasMaxLength(600);
            builder.Property(x => x.VangMat).HasDefaultValue(VangMat.CoMat);

            builder.HasOne(x => x.SinhVien)
                .WithMany(x => x.ChiTietDiemDanhs)
                .HasForeignKey(x => x.SinhVienId);

            builder.HasOne(x => x.DiemDanh)
                .WithMany(x => x.ChiTietDiemDanhs)
                .HasForeignKey(x => x.DiemDanhId);
        }
    }
}
