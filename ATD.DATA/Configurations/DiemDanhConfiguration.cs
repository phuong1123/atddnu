﻿using System;
using System.Collections.Generic;
using System.Text;
using ATD.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ATD.Data.Configurations
{
    public class DiemDanhConfiguration : IEntityTypeConfiguration<DiemDanh>
    {
        public void Configure(EntityTypeBuilder<DiemDanh> builder)
        {
            builder.ToTable("DiemDanhs");
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).UseIdentityColumn();
            builder.Property(x => x.CaHoc).IsRequired();
            builder.Property(x => x.NoiDungBuoiHoc).HasMaxLength(600);
            builder.Property(x => x.DanhGiaBuoiHoc).HasMaxLength(400);
            builder.Property(x => x.GhiChu).HasMaxLength(600);

            builder.HasOne(x => x.PhanCongGiangDay)
                .WithMany(x => x.DiemDanhs)
                .HasForeignKey(x => x.PhanCongGiangDayId);
        }
    }
}
