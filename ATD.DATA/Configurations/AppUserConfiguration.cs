﻿using System;
using System.Collections.Generic;
using System.Text;
using ATD.Data.Entities;
using ATD.Data.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ATD.Data.Configurations
{
    public class AppUserConfiguration : IEntityTypeConfiguration<AppUser>
    {
        public void Configure(EntityTypeBuilder<AppUser> builder)
        {
            builder.ToTable("AppUsers");
            builder.Property(x=>x.MaNhanVien).IsRequired().HasMaxLength(30);
            builder.Property(x => x.HoDem).IsRequired().HasMaxLength(100);
            builder.Property(x => x.TrangThaiHoatDong).HasDefaultValue(TrangThaiHoatDong.DangHoatDong);
            

            builder.HasOne(x => x.Khoa)
                .WithMany(x => x.AppUsers)
                .HasForeignKey(x => x.KhoaId);
        }
    }
}
