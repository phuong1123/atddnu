﻿using System;
using System.Collections.Generic;
using System.Text;
using ATD.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ATD.Data.Configurations
{
    public class NamHocConfiguration: IEntityTypeConfiguration<NamHoc>
    {
        public void Configure(EntityTypeBuilder<NamHoc> builder)
        {
            builder.ToTable("NamHocs");
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).UseIdentityColumn();
            builder.Property(x => x.TenNamHoc).IsRequired().HasMaxLength(100);
            builder.Property(x => x.GhiChu).HasMaxLength(450);
        }
    }
}
