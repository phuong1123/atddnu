﻿using System;
using System.Collections.Generic;
using System.Text;
using ATD.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ATD.Data.Configurations
{
    public class PhanCongGiangDayConfiguration : IEntityTypeConfiguration<PhanCongGiangDay>
    {
        public void Configure(EntityTypeBuilder<PhanCongGiangDay> builder)
        {
            builder.ToTable("PhanCongGiangDays");
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).UseIdentityColumn();
            builder.Property(x => x.HocKy).IsRequired();

            builder.HasOne(x => x.GiangVienAppUser)
                .WithMany(x => x.PhanCongGiangDays)
                .HasForeignKey(x => x.GiangVienId);

            builder.HasOne(x => x.Lop)
                .WithMany(x => x.PhanCongGiangDays)
                .HasForeignKey(x => x.LopId);

            builder.HasOne(x => x.PhongHoc)
                .WithMany(x => x.PhanCongGiangDays)
                .HasForeignKey(x => x.PhongId);

            builder.HasOne(x => x.MonHoc)
                .WithMany(x => x.PhanCongGiangDays)
                .HasForeignKey(x => x.MonHocId);

            builder.HasOne(x => x.NamHoc)
                .WithMany(x => x.PhanCongGiangDays)
                .HasForeignKey(x => x.NamHocId);
        }
    }
}
