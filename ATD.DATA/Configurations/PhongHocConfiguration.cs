﻿using System;
using System.Collections.Generic;
using System.Text;
using ATD.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ATD.Data.Configurations
{
   public class PhongHocConfiguration : IEntityTypeConfiguration<PhongHoc>
    {
        public void Configure(EntityTypeBuilder<PhongHoc> builder)
        {
            builder.ToTable("PhongHocs");
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).UseIdentityColumn();
            builder.Property(x => x.TenPhong).IsRequired().HasMaxLength(200);
            builder.Property(x => x.GhiChu).HasMaxLength(600);
        }
    }
}
