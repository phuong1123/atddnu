﻿using System;
using System.Collections.Generic;
using System.Text;
using ATD.Data.Entities;
using ATD.Data.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ATD.Data.Configurations
{
   public class SinhVienConfiguration : IEntityTypeConfiguration<SinhVien>
    {
        public void Configure(EntityTypeBuilder<SinhVien> builder)
        {
            builder.ToTable("SinhViens");
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).UseIdentityColumn();
            builder.Property(x => x.MaSinhVien).IsRequired();
            builder.Property(x => x.HoDem).IsRequired().HasMaxLength(100);
            builder.Property(x => x.Ten).IsRequired().HasMaxLength(50);
            builder.Property(x => x.Sdt).HasMaxLength(20);
            builder.Property(x => x.Email).HasMaxLength(100);
            builder.Property(x => x.DiaChi).HasMaxLength(300);
            builder.Property(x => x.GioiTinh).HasDefaultValue(GioiTinh.Nam);

            builder.HasOne(x => x.Lop)
                .WithMany(x => x.SinhViens)
                .HasForeignKey(x => x.LopId);
        }
    }
}
