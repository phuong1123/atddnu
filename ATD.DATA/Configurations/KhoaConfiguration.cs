﻿using System;
using System.Collections.Generic;
using System.Text;
using ATD.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ATD.Data.Configurations
{
    public class KhoaConfiguration : IEntityTypeConfiguration<Khoa>
    {
        public void Configure(EntityTypeBuilder<Khoa> builder)
        {
            builder.ToTable("Khoas");
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).UseIdentityColumn();
            builder.Property(x => x.TenKhoa).IsRequired().HasMaxLength(200);
            builder.Property(x => x.Sdt).HasMaxLength(20);
            builder.Property(x => x.MoTa).HasMaxLength(600);
        }
    }
}
