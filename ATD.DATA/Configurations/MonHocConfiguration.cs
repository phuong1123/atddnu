﻿using System;
using System.Collections.Generic;
using System.Text;
using ATD.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ATD.Data.Configurations
{
    public class MonHocConfiguration : IEntityTypeConfiguration<MonHoc>
    {
        public void Configure(EntityTypeBuilder<MonHoc> builder)
        {
            builder.ToTable("MonHocs");
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).UseIdentityColumn();
            builder.Property(x => x.TenMon).IsRequired();
            builder.Property(x => x.SoTinChi).IsRequired();
            builder.Property(x=>x.GhiChu).HasMaxLength(600);

            builder.HasOne(x => x.Khoa)
                .WithMany(x => x.MonHocs)
                .HasForeignKey(x => x.KhoaPhuTrachId);
        }
    }
}
