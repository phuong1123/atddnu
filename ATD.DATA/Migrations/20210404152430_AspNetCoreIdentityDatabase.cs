﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ATD.Data.Migrations
{
    public partial class AspNetCoreIdentityDatabase : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "GioiTinh",
                table: "SinhViens",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(bool),
                oldType: "bit",
                oldDefaultValue: true);

            migrationBuilder.CreateTable(
                name: "AppRoleClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    RoleId = table.Column<Guid>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AppRoleClaims", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AppRoles",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    NormalizedName = table.Column<string>(nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AppRoles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AppUserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<Guid>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AppUserClaims", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AppUserLogins",
                columns: table => new
                {
                    UserId = table.Column<Guid>(nullable: false),
                    LoginProvider = table.Column<string>(nullable: true),
                    ProviderKey = table.Column<string>(nullable: true),
                    ProviderDisplayName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AppUserLogins", x => x.UserId);
                });

            migrationBuilder.CreateTable(
                name: "AppUserRoles",
                columns: table => new
                {
                    UserId = table.Column<Guid>(nullable: false),
                    RoleId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AppUserRoles", x => new { x.RoleId, x.UserId });
                });

            migrationBuilder.CreateTable(
                name: "AppUsers",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    UserName = table.Column<string>(nullable: true),
                    NormalizedUserName = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    NormalizedEmail = table.Column<string>(nullable: true),
                    EmailConfirmed = table.Column<bool>(nullable: false),
                    PasswordHash = table.Column<string>(nullable: true),
                    SecurityStamp = table.Column<string>(nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(nullable: false),
                    TwoFactorEnabled = table.Column<bool>(nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(nullable: true),
                    LockoutEnabled = table.Column<bool>(nullable: false),
                    AccessFailedCount = table.Column<int>(nullable: false),
                    MaNhanVien = table.Column<string>(maxLength: 30, nullable: false),
                    HoDem = table.Column<string>(maxLength: 100, nullable: false),
                    Ten = table.Column<string>(nullable: false),
                    KhoaId = table.Column<int>(nullable: true),
                    NgaySinh = table.Column<DateTime>(nullable: true),
                    TrinhDo = table.Column<string>(nullable: true),
                    ChuyenMonGiangDay = table.Column<string>(nullable: true),
                    LoaiGiangVien = table.Column<int>(nullable: false, defaultValue: 0),
                    TrangThaiHoatDong = table.Column<int>(nullable: false, defaultValue: 0)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AppUsers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AppUsers_Khoas_KhoaId",
                        column: x => x.KhoaId,
                        principalTable: "Khoas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AppUserTokens",
                columns: table => new
                {
                    UserId = table.Column<Guid>(nullable: false),
                    LoginProvider = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AppUserTokens", x => x.UserId);
                });

            migrationBuilder.CreateTable(
                name: "PhanCongGiangDays",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    GiangVienId = table.Column<Guid>(nullable: false),
                    LopId = table.Column<int>(nullable: false),
                    PhongId = table.Column<int>(nullable: false),
                    MonHocId = table.Column<int>(nullable: false),
                    NamHocId = table.Column<int>(nullable: false),
                    HocKy = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PhanCongGiangDays", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PhanCongGiangDays_AppUsers_GiangVienId",
                        column: x => x.GiangVienId,
                        principalTable: "AppUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_PhanCongGiangDays_Lops_LopId",
                        column: x => x.LopId,
                        principalTable: "Lops",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_PhanCongGiangDays_MonHocs_MonHocId",
                        column: x => x.MonHocId,
                        principalTable: "MonHocs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_PhanCongGiangDays_NamHocs_NamHocId",
                        column: x => x.NamHocId,
                        principalTable: "NamHocs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_PhanCongGiangDays_PhongHocs_PhongId",
                        column: x => x.PhongId,
                        principalTable: "PhongHocs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateTable(
                name: "DiemDanhs",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    NgayDiemDanh = table.Column<DateTime>(nullable: false),
                    PhanCongGiangDayId = table.Column<int>(nullable: false),
                    CaHoc = table.Column<int>(nullable: false, defaultValue: 0),
                    GhiChu = table.Column<string>(maxLength: 600, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DiemDanhs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DiemDanhs_PhanCongGiangDays_PhanCongGiangDayId",
                        column: x => x.PhanCongGiangDayId,
                        principalTable: "PhanCongGiangDays",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ChiTietDiemDanhs",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DiemDanhId = table.Column<int>(nullable: false),
                    SinhVienId = table.Column<int>(nullable: false),
                    VangMat = table.Column<int>(nullable: false, defaultValue: 1),
                    LyDo = table.Column<string>(maxLength:600,nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ChiTietDiemDanhs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ChiTietDiemDanhs_DiemDanhs_DiemDanhId",
                        column: x => x.DiemDanhId,
                        principalTable: "DiemDanhs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ChiTietDiemDanhs_SinhViens_SinhVienId",
                        column: x => x.SinhVienId,
                        principalTable: "SinhViens",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.UpdateData(
                table: "SinhViens",
                keyColumn: "Id",
                keyValue: 1,
                column: "GioiTinh",
                value: 1);

            migrationBuilder.UpdateData(
                table: "SinhViens",
                keyColumn: "Id",
                keyValue: 2,
                column: "GioiTinh",
                value: 1);

            migrationBuilder.UpdateData(
                table: "SinhViens",
                keyColumn: "Id",
                keyValue: 3,
                column: "GioiTinh",
                value: 0);

            migrationBuilder.CreateIndex(
                name: "IX_AppUsers_KhoaId",
                table: "AppUsers",
                column: "KhoaId");

            migrationBuilder.CreateIndex(
                name: "IX_ChiTietDiemDanhs_DiemDanhId",
                table: "ChiTietDiemDanhs",
                column: "DiemDanhId");

            migrationBuilder.CreateIndex(
                name: "IX_ChiTietDiemDanhs_SinhVienId",
                table: "ChiTietDiemDanhs",
                column: "SinhVienId");

            migrationBuilder.CreateIndex(
                name: "IX_DiemDanhs_PhanCongGiangDayId",
                table: "DiemDanhs",
                column: "PhanCongGiangDayId");

            migrationBuilder.CreateIndex(
                name: "IX_PhanCongGiangDays_GiangVienId",
                table: "PhanCongGiangDays",
                column: "GiangVienId");

            migrationBuilder.CreateIndex(
                name: "IX_PhanCongGiangDays_LopId",
                table: "PhanCongGiangDays",
                column: "LopId");

            migrationBuilder.CreateIndex(
                name: "IX_PhanCongGiangDays_MonHocId",
                table: "PhanCongGiangDays",
                column: "MonHocId");

            migrationBuilder.CreateIndex(
                name: "IX_PhanCongGiangDays_NamHocId",
                table: "PhanCongGiangDays",
                column: "NamHocId");

            migrationBuilder.CreateIndex(
                name: "IX_PhanCongGiangDays_PhongId",
                table: "PhanCongGiangDays",
                column: "PhongId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AppRoleClaims");

            migrationBuilder.DropTable(
                name: "AppRoles");

            migrationBuilder.DropTable(
                name: "AppUserClaims");

            migrationBuilder.DropTable(
                name: "AppUserLogins");

            migrationBuilder.DropTable(
                name: "AppUserRoles");

            migrationBuilder.DropTable(
                name: "AppUserTokens");

            migrationBuilder.DropTable(
                name: "ChiTietDiemDanhs");

            migrationBuilder.DropTable(
                name: "DiemDanhs");

            migrationBuilder.DropTable(
                name: "PhanCongGiangDays");

            migrationBuilder.DropTable(
                name: "AppUsers");

            migrationBuilder.AlterColumn<bool>(
                name: "GioiTinh",
                table: "SinhViens",
                type: "bit",
                nullable: false,
                defaultValue: true,
                oldClrType: typeof(int),
                oldDefaultValue: 0);

            migrationBuilder.UpdateData(
                table: "SinhViens",
                keyColumn: "Id",
                keyValue: 1,
                column: "GioiTinh",
                value: false);

            migrationBuilder.UpdateData(
                table: "SinhViens",
                keyColumn: "Id",
                keyValue: 2,
                column: "GioiTinh",
                value: false);

            migrationBuilder.UpdateData(
                table: "SinhViens",
                keyColumn: "Id",
                keyValue: 3,
                column: "GioiTinh",
                value: true);
        }
    }
}
