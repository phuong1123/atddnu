﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ATD.Data.Migrations
{
    public partial class SeedingDiemDanhData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AppRoles",
                keyColumn: "Id",
                keyValue: new Guid("3f397518-6f28-4c2c-8aea-3b059df9af88"),
                column: "ConcurrencyStamp",
                value: "845e1e32-46c6-4dd1-9cdf-1bd8cb8806ac");

            migrationBuilder.UpdateData(
                table: "AppRoles",
                keyColumn: "Id",
                keyValue: new Guid("9afbe2b1-817d-4a73-b21a-e11f5181e46b"),
                column: "ConcurrencyStamp",
                value: "53be8d46-41b8-49a5-be60-180c12b26878");

            migrationBuilder.UpdateData(
                table: "AppUsers",
                keyColumn: "Id",
                keyValue: new Guid("6cda4083-cda9-42f5-84bf-fa036e191c39"),
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "3565d444-7fa8-4ccf-921a-3906409241a4", "AQAAAAEAACcQAAAAEBXtU6m6kO97xcv+qlV9truc20ZUZ+G2h27KxDdJ9VDE2Gabi4JueMtvUA9woT77Ew==" });

            migrationBuilder.UpdateData(
                table: "AppUsers",
                keyColumn: "Id",
                keyValue: new Guid("d9145e68-7b13-48d8-8271-e3c81dd289b1"),
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "6bb94b31-7f83-4c8d-9f4a-8d6e8b477822", "AQAAAAEAACcQAAAAEA1KNv7W3WnSXD4km+aCGsz23hGu6nOHQ7WL5/XtES3xyRH8gfFZORvKc7WgZiWimw==" });

            migrationBuilder.InsertData(
                table: "PhanCongGiangDays",
                columns: new[] { "Id", "GiangVienId", "HocKy", "LopId", "MonHocId", "NamHocId", "PhongId" },
                values: new object[,]
                {
                    { 1, new Guid("6cda4083-cda9-42f5-84bf-fa036e191c39"), 0, 1, 3, 4, 1 },
                    { 2, new Guid("6cda4083-cda9-42f5-84bf-fa036e191c39"), 0, 2, 3, 4, 3 },
                    { 3, new Guid("6cda4083-cda9-42f5-84bf-fa036e191c39"), 1, 1, 1, 4, 4 },
                    { 4, new Guid("d9145e68-7b13-48d8-8271-e3c81dd289b1"), 0, 5, 4, 3, 2 }
                });

            migrationBuilder.InsertData(
                table: "DiemDanhs",
                columns: new[] { "Id", "CaHoc", "DanhGiaBuoiHoc", "GhiChu", "NgayDiemDanh", "NoiDungBuoiHoc", "PhanCongGiangDayId" },
                values: new object[,]
                {
                    { 1, 0, "Tốt", "", new DateTime(2020, 5, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), "Làm quen với Java", 1 },
                    { 5, 0, "Tốt", "", new DateTime(2020, 3, 5, 0, 0, 0, 0, DateTimeKind.Unspecified), "Nguyên lý thống kê", 1 },
                    { 2, 0, "Khá", "", new DateTime(2020, 3, 4, 0, 0, 0, 0, DateTimeKind.Unspecified), "Phương pháp giải tuyến tính", 2 },
                    { 3, 1, "Tốt", "", new DateTime(2020, 3, 5, 0, 0, 0, 0, DateTimeKind.Unspecified), "OOP là gì?", 3 },
                    { 4, 0, "Khá", "", new DateTime(2020, 3, 5, 0, 0, 0, 0, DateTimeKind.Unspecified), "Làm quen với ngôn ngữ lập trình", 4 }
                });

            migrationBuilder.InsertData(
                table: "ChiTietDiemDanhs",
                columns: new[] { "Id", "DiemDanhId", "LyDo", "SinhVienId", "VangMat" },
                values: new object[,]
                {
                    { 1, 1, "", 1, 1 },
                    { 2, 1, "", 2, 1 }
                });

            migrationBuilder.InsertData(
                table: "ChiTietDiemDanhs",
                columns: new[] { "Id", "DiemDanhId", "LyDo", "SinhVienId" },
                values: new object[] { 3, 1, "", 3 });

            migrationBuilder.InsertData(
                table: "ChiTietDiemDanhs",
                columns: new[] { "Id", "DiemDanhId", "LyDo", "SinhVienId", "VangMat" },
                values: new object[,]
                {
                    { 4, 2, "", 1, 2 },
                    { 5, 2, "", 2, 1 },
                    { 6, 2, "", 3, 1 }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "ChiTietDiemDanhs",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "ChiTietDiemDanhs",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "ChiTietDiemDanhs",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "ChiTietDiemDanhs",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "ChiTietDiemDanhs",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "ChiTietDiemDanhs",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "DiemDanhs",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "DiemDanhs",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "DiemDanhs",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "DiemDanhs",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "DiemDanhs",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "PhanCongGiangDays",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "PhanCongGiangDays",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "PhanCongGiangDays",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "PhanCongGiangDays",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.UpdateData(
                table: "AppRoles",
                keyColumn: "Id",
                keyValue: new Guid("3f397518-6f28-4c2c-8aea-3b059df9af88"),
                column: "ConcurrencyStamp",
                value: "f3568e92-e7f2-4177-a492-1896ad69e6ab");

            migrationBuilder.UpdateData(
                table: "AppRoles",
                keyColumn: "Id",
                keyValue: new Guid("9afbe2b1-817d-4a73-b21a-e11f5181e46b"),
                column: "ConcurrencyStamp",
                value: "eb38a800-826e-4b87-a64c-ed8e54e182c9");

            migrationBuilder.UpdateData(
                table: "AppUsers",
                keyColumn: "Id",
                keyValue: new Guid("6cda4083-cda9-42f5-84bf-fa036e191c39"),
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "f634b916-7fe7-47f0-b0ef-709d562836ef", "AQAAAAEAACcQAAAAENHZMKksQAYNEf0U34DclFY9OUhvr+d8gnHm/xdn7hBX16pIQzY6jeMM67t74NcRYg==" });

            migrationBuilder.UpdateData(
                table: "AppUsers",
                keyColumn: "Id",
                keyValue: new Guid("d9145e68-7b13-48d8-8271-e3c81dd289b1"),
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "a605f870-d456-4199-908c-1dd0b16d0f2e", "AQAAAAEAACcQAAAAEOd38f9LFXv6WK4Hum3yHv1xeVfVkdGNkEKzsqFKFghrvBObI4fwHdXMutuIxaAzUg==" });
        }
    }
}
