﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ATD.Data.Migrations
{
    public partial class Update_Tbl_DiemDanh : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "DanhGiaBuoiHoc",
                table: "DiemDanhs",
                maxLength: 400,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "NoiDungBuoiHoc",
                table: "DiemDanhs",
                maxLength: 600,
                nullable: true);

            migrationBuilder.UpdateData(
                table: "AppRoles",
                keyColumn: "Id",
                keyValue: new Guid("3f397518-6f28-4c2c-8aea-3b059df9af88"),
                column: "ConcurrencyStamp",
                value: "f3568e92-e7f2-4177-a492-1896ad69e6ab");

            migrationBuilder.UpdateData(
                table: "AppRoles",
                keyColumn: "Id",
                keyValue: new Guid("9afbe2b1-817d-4a73-b21a-e11f5181e46b"),
                column: "ConcurrencyStamp",
                value: "eb38a800-826e-4b87-a64c-ed8e54e182c9");

            migrationBuilder.UpdateData(
                table: "AppUsers",
                keyColumn: "Id",
                keyValue: new Guid("6cda4083-cda9-42f5-84bf-fa036e191c39"),
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "f634b916-7fe7-47f0-b0ef-709d562836ef", "AQAAAAEAACcQAAAAENHZMKksQAYNEf0U34DclFY9OUhvr+d8gnHm/xdn7hBX16pIQzY6jeMM67t74NcRYg==" });

            migrationBuilder.UpdateData(
                table: "AppUsers",
                keyColumn: "Id",
                keyValue: new Guid("d9145e68-7b13-48d8-8271-e3c81dd289b1"),
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "a605f870-d456-4199-908c-1dd0b16d0f2e", "AQAAAAEAACcQAAAAEOd38f9LFXv6WK4Hum3yHv1xeVfVkdGNkEKzsqFKFghrvBObI4fwHdXMutuIxaAzUg==" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DanhGiaBuoiHoc",
                table: "DiemDanhs");

            migrationBuilder.DropColumn(
                name: "NoiDungBuoiHoc",
                table: "DiemDanhs");

            migrationBuilder.UpdateData(
                table: "AppRoles",
                keyColumn: "Id",
                keyValue: new Guid("3f397518-6f28-4c2c-8aea-3b059df9af88"),
                column: "ConcurrencyStamp",
                value: "934f498e-2b03-4239-9fd6-e3a08dc6712c");

            migrationBuilder.UpdateData(
                table: "AppRoles",
                keyColumn: "Id",
                keyValue: new Guid("9afbe2b1-817d-4a73-b21a-e11f5181e46b"),
                column: "ConcurrencyStamp",
                value: "781db5fc-dd3f-44bf-af1a-faf4d9c4015b");

            migrationBuilder.UpdateData(
                table: "AppUsers",
                keyColumn: "Id",
                keyValue: new Guid("6cda4083-cda9-42f5-84bf-fa036e191c39"),
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "4493937d-8642-4ec3-bc1e-146e5674948d", "AQAAAAEAACcQAAAAEACwI5/BKd5q45cT8NNTgiBHns9aFjQX3pcYHJVJ+PWg67xiETr3qCabZg65H0DEvw==" });

            migrationBuilder.UpdateData(
                table: "AppUsers",
                keyColumn: "Id",
                keyValue: new Guid("d9145e68-7b13-48d8-8271-e3c81dd289b1"),
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "c0001eb0-f092-42b8-90dd-29c3e113d901", "AQAAAAEAACcQAAAAELSzj3A+LrNbsmfa8x/8vCvSp4JPcarhfubTPOOQ3y6H4AZ4d6H/l7xBhs1xMNYz+Q==" });
        }
    }
}
