﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ATD.Data.Migrations
{
    public partial class SeedingIdentityCoreData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "GhiChu",
                table: "DiemDanhs",
                maxLength: 600,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "VangMat",
                table: "ChiTietDiemDanhs",
                nullable: false,
                defaultValue: 1,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<string>(
                name: "LyDo",
                table: "ChiTietDiemDanhs",
                maxLength: 600,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.InsertData(
                table: "AppRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Description", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { new Guid("9afbe2b1-817d-4a73-b21a-e11f5181e46b"), "781db5fc-dd3f-44bf-af1a-faf4d9c4015b", "Administator role", "admin", "admin" },
                    { new Guid("3f397518-6f28-4c2c-8aea-3b059df9af88"), "934f498e-2b03-4239-9fd6-e3a08dc6712c", "Partner role", "ctsv", "ctsv" }
                });

            migrationBuilder.InsertData(
                table: "AppUserRoles",
                columns: new[] { "RoleId", "UserId" },
                values: new object[,]
                {
                    { new Guid("9afbe2b1-817d-4a73-b21a-e11f5181e46b"), new Guid("6cda4083-cda9-42f5-84bf-fa036e191c39") },
                    { new Guid("3f397518-6f28-4c2c-8aea-3b059df9af88"), new Guid("d9145e68-7b13-48d8-8271-e3c81dd289b1") }
                });

            migrationBuilder.InsertData(
                table: "AppUsers",
                columns: new[] { "Id", "AccessFailedCount", "ChuyenMonGiangDay", "ConcurrencyStamp", "Email", "EmailConfirmed", "HoDem", "KhoaId", "LoaiGiangVien", "LockoutEnabled", "LockoutEnd", "MaNhanVien", "NgaySinh", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "Ten", "TrinhDo", "TwoFactorEnabled", "UserName" },
                values: new object[,]
                {
                    { new Guid("6cda4083-cda9-42f5-84bf-fa036e191c39"), 0, "Tin Học", "4493937d-8642-4ec3-bc1e-146e5674948d", "tiepphamvank21@gmail.com", true, "Phạm Văn", 1, 0, false, null, "ABC123456", new DateTime(1984, 6, 6, 0, 0, 0, 0, DateTimeKind.Unspecified), "tiepphamvank21@gmail.com", "admin", "AQAAAAEAACcQAAAAEACwI5/BKd5q45cT8NNTgiBHns9aFjQX3pcYHJVJ+PWg67xiETr3qCabZg65H0DEvw==", null, false, "", "Tiệp", "Thạc Sỹ", false, "admin" },
                    { new Guid("d9145e68-7b13-48d8-8271-e3c81dd289b1"), 0, "", "c0001eb0-f092-42b8-90dd-29c3e113d901", "phongctsv@gmail.com", true, "Nguyễn Trần Văn", 1, 0, false, null, "CTX123456", new DateTime(1990, 5, 24, 0, 0, 0, 0, DateTimeKind.Unspecified), "phongctsv@gmail.com", "ctsv", "AQAAAAEAACcQAAAAELSzj3A+LrNbsmfa8x/8vCvSp4JPcarhfubTPOOQ3y6H4AZ4d6H/l7xBhs1xMNYz+Q==", null, false, "", "Nguyên", "Cử Nhân", false, "ctsv" }
                });

            migrationBuilder.InsertData(
                table: "Khoas",
                columns: new[] { "Id", "Email", "MoTa", "Sdt", "TenKhoa" },
                values: new object[] { 3, "PhongCTSV@gmail.com", "", "0379452201", "Phòng Công tác sinh viên" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AppRoles",
                keyColumn: "Id",
                keyValue: new Guid("3f397518-6f28-4c2c-8aea-3b059df9af88"));

            migrationBuilder.DeleteData(
                table: "AppRoles",
                keyColumn: "Id",
                keyValue: new Guid("9afbe2b1-817d-4a73-b21a-e11f5181e46b"));

            migrationBuilder.DeleteData(
                table: "AppUserRoles",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { new Guid("3f397518-6f28-4c2c-8aea-3b059df9af88"), new Guid("d9145e68-7b13-48d8-8271-e3c81dd289b1") });

            migrationBuilder.DeleteData(
                table: "AppUserRoles",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { new Guid("9afbe2b1-817d-4a73-b21a-e11f5181e46b"), new Guid("6cda4083-cda9-42f5-84bf-fa036e191c39") });

            migrationBuilder.DeleteData(
                table: "AppUsers",
                keyColumn: "Id",
                keyValue: new Guid("6cda4083-cda9-42f5-84bf-fa036e191c39"));

            migrationBuilder.DeleteData(
                table: "AppUsers",
                keyColumn: "Id",
                keyValue: new Guid("d9145e68-7b13-48d8-8271-e3c81dd289b1"));

            migrationBuilder.DeleteData(
                table: "Khoas",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.AlterColumn<string>(
                name: "GhiChu",
                table: "DiemDanhs",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 600,
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "VangMat",
                table: "ChiTietDiemDanhs",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldDefaultValue: 1);

            migrationBuilder.AlterColumn<string>(
                name: "LyDo",
                table: "ChiTietDiemDanhs",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 600,
                oldNullable: true);
        }
    }
}
