﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ATD.Data.Migrations
{
    public partial class SeedData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "AppConfigs",
                columns: new[] { "Key", "Value" },
                values: new object[,]
                {
                    { "HomePage", "This is the first page of Atd" },
                    { "HomeDescription", "This is the description of Atd" }
                });

            migrationBuilder.InsertData(
                table: "Khoas",
                columns: new[] { "Id", "Email", "MoTa", "Sdt", "TenKhoa" },
                values: new object[,]
                {
                    { 1, "KhoaCNTT@gmail.com", "", "0965630621", "Khoa Công Nghệ Thông Tin" },
                    { 2, "KhoaTCNH@gmail.com", "", "0942558800", "Khoa Tài Chính Ngân Hàng" }
                });

            migrationBuilder.InsertData(
                table: "NamHocs",
                columns: new[] { "Id", "GhiChu", "TenNamHoc" },
                values: new object[,]
                {
                    { 1, "Kỷ niệm 10 năm TL trường", "2017 - 2018" },
                    { 2, "", "2018 - 2019" },
                    { 3, "", "2019 - 2020" },
                    { 4, "", "2019 - 2020" },
                    { 5, "", "2020 - 2021" }
                });

            migrationBuilder.InsertData(
                table: "PhongHocs",
                columns: new[] { "Id", "GhiChu", "SoChoNgoi", "TenPhong" },
                values: new object[,]
                {
                    { 1, "Khoa CNTT", 30, "LAB-01" },
                    { 2, "Phòng kỹ năng mềm", 60, "P101" },
                    { 3, "", 40, "P102" },
                    { 4, "", 40, "P103" },
                    { 5, "", 50, "P201" }
                });

            migrationBuilder.InsertData(
                table: "Lops",
                columns: new[] { "Id", "KhoaId", "SiSo", "TenLop" },
                values: new object[,]
                {
                    { 1, 1, 15, "CNTT11-01" },
                    { 2, 1, 35, "CNTT12-01" },
                    { 3, 1, 26, "CNTT12-02" },
                    { 4, 1, 28, "CNTT12-03" },
                    { 5, 2, 10, "TCNH11-01" }
                });

            migrationBuilder.InsertData(
                table: "MonHocs",
                columns: new[] { "Id", "GhiChu", "KhoaPhuTrachId", "SoTinChi", "TenMon" },
                values: new object[,]
                {
                    { 1, "", 1, 3, "Toán Cao Cấp" },
                    { 2, "", 1, 3, "Toán Rời Rạc" },
                    { 3, "", 1, 4, "Lập trình hướng đối tượng" },
                    { 4, "", 2, 3, "Nguyên lý kế toán" }
                });

            migrationBuilder.InsertData(
                table: "SinhViens",
                columns: new[] { "Id", "DiaChi", "Email", "HinhAnh", "HoDem", "LopId", "MaSinhVien", "NgaySinh", "Sdt", "Ten" },
                values: new object[] { 1, "Hà Nội", "", "", "Nguyễn Thị Thu", 1, "1151020028", new DateTime(1999, 5, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), "", "Phương" });

            migrationBuilder.InsertData(
                table: "SinhViens",
                columns: new[] { "Id", "DiaChi", "Email", "HinhAnh", "HoDem", "LopId", "MaSinhVien", "NgaySinh", "Sdt", "Ten" },
                values: new object[] { 2, "Nam Định", "hoa2412nd@gmail.com", "", "Nguyễn Thị", 1, "1151020014", new DateTime(1997, 12, 24, 0, 0, 0, 0, DateTimeKind.Unspecified), "0337892505", "Hoa" });

            migrationBuilder.InsertData(
                table: "SinhViens",
                columns: new[] { "Id", "DiaChi", "Email", "GioiTinh", "HinhAnh", "HoDem", "LopId", "MaSinhVien", "NgaySinh", "Sdt", "Ten" },
                values: new object[] { 3, "Thái Nguyên", "", true, "", "Nguyễn Khánh", 5, "1151020028", new DateTime(1999, 6, 29, 0, 0, 0, 0, DateTimeKind.Unspecified), "", "Nguyên" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AppConfigs",
                keyColumn: "Key",
                keyValue: "HomeDescription");

            migrationBuilder.DeleteData(
                table: "AppConfigs",
                keyColumn: "Key",
                keyValue: "HomePage");

            migrationBuilder.DeleteData(
                table: "Lops",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Lops",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Lops",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "MonHocs",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "MonHocs",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "MonHocs",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "MonHocs",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "NamHocs",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "NamHocs",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "NamHocs",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "NamHocs",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "NamHocs",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "PhongHocs",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "PhongHocs",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "PhongHocs",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "PhongHocs",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "PhongHocs",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "SinhViens",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "SinhViens",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "SinhViens",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Lops",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Lops",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Khoas",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Khoas",
                keyColumn: "Id",
                keyValue: 2);
        }
    }
}
