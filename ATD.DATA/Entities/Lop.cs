﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ATD.Data.Entities
{
    public class Lop
    {
        public int Id { get; set; }
        public string TenLop { get; set; }
        public int SiSo { get; set; }
        public int KhoaId { get; set; }
        public virtual Khoa Khoa { get; set; }
        public virtual List<SinhVien> SinhViens { get; set; }
        public virtual List<PhanCongGiangDay> PhanCongGiangDays { get; set; }
    }
}
