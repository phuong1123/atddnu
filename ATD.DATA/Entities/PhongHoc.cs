﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ATD.Data.Entities
{
    public class PhongHoc
    {
        public int Id { get; set; }
        public string TenPhong { get; set; }
        public int SoChoNgoi { get; set; }
        public string GhiChu { get; set; }

        public virtual List<PhanCongGiangDay> PhanCongGiangDays { get; set; }
    }
}
