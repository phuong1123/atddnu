﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ATD.Data.Entities
{
    public class NamHoc
    {
        public int Id { get; set; }
        public string TenNamHoc { get; set; }
        public string GhiChu { get; set; }
        public virtual List<PhanCongGiangDay> PhanCongGiangDays { get; set; }
    }
}
