﻿using System;
using System.Collections.Generic;
using System.Text;
using ATD.Data.Enums;

namespace ATD.Data.Entities
{
    public class PhanCongGiangDay
    {
        public int Id { get; set; }
        public Guid GiangVienId  { get; set; }
        public int LopId  { get; set; }
        public int PhongId  { get; set; }
        public int MonHocId  { get; set; }
        public int NamHocId  { get; set; }
        public HocKy HocKy  { get; set; }

        public virtual AppUser GiangVienAppUser { get; set; }
        public virtual Lop Lop { get; set; }
        public virtual PhongHoc PhongHoc { get; set; }
        public virtual MonHoc MonHoc { get; set; }
        public virtual NamHoc NamHoc { get; set; }

        public  virtual  List<DiemDanh> DiemDanhs { get; set; }
    }
}
