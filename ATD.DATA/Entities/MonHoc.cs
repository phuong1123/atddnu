﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ATD.Data.Entities
{
    public class MonHoc
    {
        public int Id { get; set; }
        public string TenMon { get; set; }
        public int SoTinChi { get; set; }
        public int KhoaPhuTrachId { get; set; }
        public string GhiChu { get; set; }

        public virtual Khoa Khoa { get; set; }
        public virtual List<PhanCongGiangDay> PhanCongGiangDays { get; set; }
    }
}
