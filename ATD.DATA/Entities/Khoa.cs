﻿using System;
using System.Collections.Generic;
using System.Text;
using ATD.Data.Enums;

namespace ATD.Data.Entities
{
    public class Khoa
    {
        public int Id { get; set; }
        public string TenKhoa { get; set; }
        public string Sdt { get; set; }
        public string Email { get; set; }
        public string MoTa { get; set; }
        public virtual List<Lop> Lops { get; set; }
        public virtual List<MonHoc> MonHocs { get; set; }

        public virtual List<AppUser> AppUsers { get; set; }
    }
}
