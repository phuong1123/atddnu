﻿using System;
using System.Collections.Generic;
using System.Text;
using ATD.Data.Enums;
using Microsoft.AspNetCore.Identity;

namespace ATD.Data.Entities
{
    public class AppUser : IdentityUser<Guid>
    {
        public string MaNhanVien { get; set; }
        public string  HoDem { get; set; }
        public string  Ten { get; set; }
        public int KhoaId { get; set; }
        public DateTime NgaySinh { get; set; }
        public string TrinhDo { get; set; }
        public string ChuyenMonGiangDay { get; set; }
        public LoaiGiangVien LoaiGiangVien { get; set; }
        public TrangThaiHoatDong TrangThaiHoatDong { get; set; }

        public virtual Khoa Khoa { get; set; }

        public virtual List<PhanCongGiangDay> PhanCongGiangDays { get; set; }
    }
}
