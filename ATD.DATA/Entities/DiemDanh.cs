﻿using System;
using System.Collections.Generic;
using System.Text;
using ATD.Data.Enums;

namespace ATD.Data.Entities
{
    public class DiemDanh
    {
        public int Id { get; set; }
        public DateTime NgayDiemDanh { get; set; }
        public int PhanCongGiangDayId { get; set; }
        public CaHoc CaHoc { get; set; }
        public string NoiDungBuoiHoc { get; set; }
        public string DanhGiaBuoiHoc { get; set; }
        public string GhiChu { get; set; }

        public virtual PhanCongGiangDay PhanCongGiangDay { get; set; }
        public  virtual  List<ChiTietDiemDanh> ChiTietDiemDanhs { get; set; }
    }
}
