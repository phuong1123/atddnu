﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using ATD.Data.Enums;

namespace ATD.Data.Entities
{
    public class SinhVien
    {
        public int Id { get; set; }
        public string MaSinhVien { get; set; }
        public string HoDem { get; set; }
        public string Ten { get; set; }
        public GioiTinh GioiTinh { get; set; }
        public int LopId { get; set; }
        public string HinhAnh { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime NgaySinh { get; set; }
        public string Sdt { get; set; }
        public string Email { get; set; }
        public string DiaChi { get; set; }

        public virtual Lop Lop { get; set; }
        public virtual List<ChiTietDiemDanh> ChiTietDiemDanhs { get; set; }
    }
}
