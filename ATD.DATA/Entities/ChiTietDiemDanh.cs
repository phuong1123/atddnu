﻿using System;
using System.Collections.Generic;
using System.Text;
using ATD.Data.Enums;

namespace ATD.Data.Entities
{
    public class ChiTietDiemDanh
    {
        public int Id { get; set; }
        public int DiemDanhId { get; set; }
        public int SinhVienId { get; set; }
        public VangMat VangMat { get; set; }
        public string LyDo { get; set; }

        public virtual DiemDanh DiemDanh { get; set; }
        public virtual SinhVien SinhVien { get; set; }
    }
}
