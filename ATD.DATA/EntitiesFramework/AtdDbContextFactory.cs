﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace ATD.Data.EntitiesFramework
{
    class AtdDbContextFactory : IDesignTimeDbContextFactory<AtdDbContext>
    {
        public AtdDbContext CreateDbContext(string[] args)
        {
            IConfigurationRoot configurationRoot = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsetting.json")
                .Build();

            var connectionString = configurationRoot.GetConnectionString("AttendanceDatabase");

            var optionBuilder = new DbContextOptionsBuilder<AtdDbContext > ();
            optionBuilder.UseSqlServer(connectionString);

            return new AtdDbContext(optionBuilder.Options);
        }
    }
}
