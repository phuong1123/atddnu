﻿using System;
using System.Collections.Generic;
using System.Text;
using ATD.Data.Configurations;
using ATD.Data.Entities;
using ATD.Data.Extensions;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace ATD.Data.EntitiesFramework
{
    public class AtdDbContext : IdentityDbContext<AppUser, AppRole, Guid>
    {
        public AtdDbContext(DbContextOptions options) : base(options)
        {
        
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //Config using Fluent API
            modelBuilder.ApplyConfiguration(new AppConfigConfiguration());
            modelBuilder.ApplyConfiguration(new PhongHocConfiguration());
            modelBuilder.ApplyConfiguration(new KhoaConfiguration());
            modelBuilder.ApplyConfiguration(new LopConfiguration());
            modelBuilder.ApplyConfiguration(new NamHocConfiguration());
            modelBuilder.ApplyConfiguration(new MonHocConfiguration());
            modelBuilder.ApplyConfiguration(new SinhVienConfiguration());
            modelBuilder.ApplyConfiguration(new AppUserConfiguration());
            modelBuilder.ApplyConfiguration(new AppRoleConfiguration());
            modelBuilder.ApplyConfiguration(new PhanCongGiangDayConfiguration());
            modelBuilder.ApplyConfiguration(new DiemDanhConfiguration());
            modelBuilder.ApplyConfiguration(new ChiTietDiemDanhConfiguration());

            modelBuilder.Entity<IdentityUserClaim<Guid>>().ToTable("AppUserClaims");
            modelBuilder.Entity<IdentityUserRole<Guid>>().ToTable("AppUserRoles").HasKey(x=> new {x.RoleId, x.UserId});
            modelBuilder.Entity<IdentityUserLogin<Guid>>().ToTable("AppUserLogins").HasKey(x=>x.UserId);
            modelBuilder.Entity<IdentityRoleClaim<Guid>>().ToTable("AppRoleClaims");
            modelBuilder.Entity<IdentityUserToken<Guid>>().ToTable("AppUserTokens").HasKey(x=>x.UserId);

            modelBuilder.Seed();
            //base.OnModelCreating(modelBuilder);
        }

        public DbSet<Khoa> Khoas { get; set; }
        public DbSet<AppConfig> AppConfigs { get; set; }
        public DbSet<PhongHoc> PhongHocs { get; set; }
        public DbSet<Lop> Lops { get; set; }
        public DbSet<NamHoc> NamHocs { get; set; }
        public DbSet<MonHoc> MonHocs { get; set; }
        public DbSet<SinhVien> SinhViens { get; set; }
        public DbSet<PhanCongGiangDay> PhanCongGiangDays { get; set; }
        public DbSet<DiemDanh> DiemDanhs { get; set; }
        public DbSet<ChiTietDiemDanh> ChiTietDiemDanhs { get; set; } 
    }
}
