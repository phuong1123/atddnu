﻿using System;
using System.Collections.Generic;
using System.Text;
using ATD.Data.Entities;
using ATD.Data.Enums;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace ATD.Data.Extensions
{
    public static class ModelBuilderExtensions
    {
        public static void Seed(this ModelBuilder modelBuilder)
        {
            //Data seeding
            modelBuilder.Entity<AppConfig>().HasData(
                new AppConfig() { Key = "HomePage", Value = "This is the first page of Atd" },
                new AppConfig() { Key = "HomeDescription", Value = "This is the description of Atd" }
            );

            modelBuilder.Entity<Khoa>().HasData(
                new Khoa() { Id =1, TenKhoa = "Khoa Công Nghệ Thông Tin", Sdt = "0965630621", Email = "KhoaCNTT@gmail.com", MoTa = ""},
                new Khoa() { Id = 2, TenKhoa = "Khoa Tài Chính Ngân Hàng", Sdt = "0942558800", Email = "KhoaTCNH@gmail.com", MoTa = ""},
                new Khoa() { Id = 3, TenKhoa = "Phòng Công tác sinh viên", Sdt = "0379452201", Email = "PhongCTSV@gmail.com", MoTa = ""}
                );

            modelBuilder.Entity<NamHoc>().HasData(
                new NamHoc() { Id = 1, TenNamHoc = "2017 - 2018", GhiChu = "Kỷ niệm 10 năm TL trường"},
                new NamHoc() { Id = 2, TenNamHoc = "2018 - 2019", GhiChu = ""},
                new NamHoc() { Id = 3, TenNamHoc = "2019 - 2020", GhiChu = ""},
                new NamHoc() { Id = 4, TenNamHoc = "2019 - 2020", GhiChu = ""},
                new NamHoc() { Id = 5, TenNamHoc = "2020 - 2021", GhiChu = ""}
                );

            modelBuilder.Entity<PhongHoc>().HasData(
                new PhongHoc() { Id = 1, TenPhong = "LAB-01", SoChoNgoi = 30, GhiChu = "Khoa CNTT"},
                new PhongHoc() { Id = 2, TenPhong = "P101", SoChoNgoi = 60, GhiChu = "Phòng kỹ năng mềm"},
                new PhongHoc() { Id = 3, TenPhong = "P102", SoChoNgoi = 40, GhiChu = ""},
                new PhongHoc() { Id = 4, TenPhong = "P103", SoChoNgoi = 40, GhiChu = ""},
                new PhongHoc() { Id = 5, TenPhong = "P201", SoChoNgoi = 50, GhiChu = ""}
            );

            modelBuilder.Entity<MonHoc>().HasData(
                new MonHoc() { Id = 1, TenMon = "Toán Cao Cấp", SoTinChi = 3, KhoaPhuTrachId = 1, GhiChu = ""},
                new MonHoc() { Id = 2, TenMon = "Toán Rời Rạc", SoTinChi = 3, KhoaPhuTrachId = 1, GhiChu = ""},
                new MonHoc() { Id = 3, TenMon = "Lập trình hướng đối tượng", SoTinChi = 4, KhoaPhuTrachId = 1, GhiChu = ""},
                new MonHoc() { Id = 4, TenMon = "Nguyên lý kế toán", SoTinChi = 3, KhoaPhuTrachId = 2, GhiChu = ""}
            );

            modelBuilder.Entity<Lop>().HasData(
                new Lop() { Id = 1, TenLop = "CNTT11-01", SiSo = 15, KhoaId = 1},
                new Lop() { Id = 2, TenLop = "CNTT12-01", SiSo = 35, KhoaId = 1},
                new Lop() { Id = 3, TenLop = "CNTT12-02", SiSo = 26, KhoaId = 1},
                new Lop() { Id = 4, TenLop = "CNTT12-03", SiSo = 28, KhoaId = 1},
                new Lop() { Id = 5, TenLop = "TCNH11-01", SiSo = 10, KhoaId = 2}
                );

            modelBuilder.Entity<SinhVien>().HasData(
                new SinhVien()
                {
                    Id = 1,
                    MaSinhVien = "1151020028",
                    HoDem = "Nguyễn Thị Thu",
                    Ten = "Phương",
                    GioiTinh = GioiTinh.Nu,
                    LopId = 1,
                    HinhAnh = "",
                    NgaySinh = new DateTime(1999, 05, 15),
                    Sdt = "",
                    Email = "",
                    DiaChi = "Hà Nội"
                },
                new SinhVien()
                {
                    Id = 2,
                    MaSinhVien = "1151020014",
                    HoDem = "Nguyễn Thị",
                    Ten = "Hoa",
                    GioiTinh = GioiTinh.Nu,
                    LopId = 1,
                    HinhAnh = "",
                    NgaySinh = new DateTime(1997, 12, 24),
                    Sdt = "0337892505",
                    Email = "hoa2412nd@gmail.com",
                    DiaChi = "Nam Định"
                },
                new SinhVien()
                {
                    Id = 3,
                    MaSinhVien = "1151020028",
                    HoDem = "Nguyễn Khánh",
                    Ten = "Nguyên",
                    GioiTinh = GioiTinh.Nam,
                    LopId = 5,
                    HinhAnh = "",
                    NgaySinh = new DateTime(1999, 06, 29),
                    Sdt = "",
                    Email = "",
                    DiaChi = "Thái Nguyên"
                }
                );

            var roleId = new Guid("9AFBE2B1-817D-4A73-B21A-E11F5181E46B");
            var adminId = new Guid("6CDA4083-CDA9-42F5-84BF-FA036E191C39");

            var roleId2 = new Guid("3F397518-6F28-4C2C-8AEA-3B059DF9AF88");
            var ctsvId = new Guid("D9145E68-7B13-48D8-8271-E3C81DD289B1");

            modelBuilder.Entity<AppRole>().HasData( new AppRole()
                {
                    Id = roleId,
                    Name = "admin",
                    NormalizedName = "admin",
                    Description = "Administator role"
                },
                new AppRole()
                {
                    Id = roleId2,
                    Name = "ctsv",
                    NormalizedName = "ctsv",
                    Description = "Partner role"
                });

            var hasher = new PasswordHasher<AppUser>();
            modelBuilder.Entity<AppUser>().HasData(new AppUser()
            {
                Id = adminId,
                UserName = "admin",
                NormalizedUserName = "admin",
                Email = "tiepphamvank21@gmail.com",
                NormalizedEmail = "tiepphamvank21@gmail.com",
                EmailConfirmed = true,
                PasswordHash = hasher.HashPassword(null, "123456"),
                SecurityStamp = string.Empty,
                MaNhanVien = "ABC123456",
                HoDem = "Phạm Văn",
                Ten = "Tiệp",
                KhoaId = 1,
                NgaySinh = new DateTime(1984,06,06),
                TrinhDo = "Thạc Sỹ",
                ChuyenMonGiangDay = "Tin Học",
                LoaiGiangVien = LoaiGiangVien.CoHuu,
                TrangThaiHoatDong = TrangThaiHoatDong.DangHoatDong
            },
            new AppUser()
            {
                Id = ctsvId,
                UserName = "ctsv",
                NormalizedUserName = "ctsv",
                Email = "phongctsv@gmail.com",
                NormalizedEmail = "phongctsv@gmail.com",
                EmailConfirmed = true,
                PasswordHash = hasher.HashPassword(null, "123456"),
                SecurityStamp = string.Empty,
                MaNhanVien = "CTX123456",
                HoDem = "Nguyễn Trần Văn",
                Ten = "Nguyên",
                KhoaId = 1,
                NgaySinh = new DateTime(1990, 05, 24),
                TrinhDo = "Cử Nhân",
                ChuyenMonGiangDay = "",
                LoaiGiangVien = LoaiGiangVien.CoHuu,
                TrangThaiHoatDong = TrangThaiHoatDong.DangHoatDong
            });

            modelBuilder.Entity<IdentityUserRole<Guid>>().HasData(new IdentityUserRole<Guid>()
            {
                RoleId = roleId,
                UserId = adminId
            },
            new IdentityUserRole<Guid>()
            {
                RoleId = roleId2,
                UserId = ctsvId
            });


            modelBuilder.Entity<PhanCongGiangDay>().HasData(
                new PhanCongGiangDay() { 
                    Id = 1, 
                    GiangVienId = adminId,
                    LopId = 1,
                    PhongId = 1,
                    MonHocId = 3,
                    NamHocId = 4,
                    HocKy = HocKy.I
                },

                new PhanCongGiangDay()
                {
                    Id = 2,
                    GiangVienId = adminId,
                    LopId = 2,
                    PhongId = 3,
                    MonHocId = 3,
                    NamHocId = 4,
                    HocKy = HocKy.I
                },

                new PhanCongGiangDay()
                {
                    Id = 3,
                    GiangVienId = adminId,
                    LopId = 1,
                    PhongId = 4,
                    MonHocId = 1,
                    NamHocId = 4,
                    HocKy = HocKy.II
                },

                new PhanCongGiangDay()
                {
                    Id = 4,
                    GiangVienId = ctsvId,
                    LopId = 5,
                    PhongId = 2,
                    MonHocId = 4,
                    NamHocId = 3,
                    HocKy = HocKy.I
                }
            );


            modelBuilder.Entity<DiemDanh>().HasData(
                new DiemDanh()
                {
                    Id = 1,
                    NgayDiemDanh = new DateTime(2020,05,15),
                    PhanCongGiangDayId = 1,
                    CaHoc = CaHoc.Sang,
                    NoiDungBuoiHoc = "Làm quen với Java",
                    DanhGiaBuoiHoc = "Tốt",
                    GhiChu = ""
                },

                new DiemDanh()
                {
                    Id = 2,
                    NgayDiemDanh = new DateTime(2020,03,04),
                    PhanCongGiangDayId = 2,
                    CaHoc = CaHoc.Sang,
                    NoiDungBuoiHoc = "Phương pháp giải tuyến tính",
                    DanhGiaBuoiHoc = "Khá",
                    GhiChu = ""
                },

                 new DiemDanh()
                 {
                     Id = 3,
                     NgayDiemDanh = new DateTime(2020, 03, 05),
                     PhanCongGiangDayId = 3,
                     CaHoc = CaHoc.Chieu,
                     NoiDungBuoiHoc = "OOP là gì?",
                     DanhGiaBuoiHoc = "Tốt",
                     GhiChu = ""
                 },

                  new DiemDanh()
                  {
                      Id = 4,
                      NgayDiemDanh = new DateTime(2020, 03, 05),
                      PhanCongGiangDayId = 4,
                      CaHoc = CaHoc.Sang,
                      NoiDungBuoiHoc = "Làm quen với ngôn ngữ lập trình",
                      DanhGiaBuoiHoc = "Khá",
                      GhiChu = ""
                  },

                   new DiemDanh()
                   {
                       Id = 5,
                       NgayDiemDanh = new DateTime(2020, 03, 05),
                       PhanCongGiangDayId = 1,
                       CaHoc = CaHoc.Sang,
                       NoiDungBuoiHoc = "Nguyên lý thống kê",
                       DanhGiaBuoiHoc = "Tốt",
                       GhiChu = ""
                   }
            );

            modelBuilder.Entity<ChiTietDiemDanh>().HasData(
                new ChiTietDiemDanh()
                {
                    Id = 1,
                    DiemDanhId = 1,
                    SinhVienId = 1,
                    VangMat = VangMat.CoMat,
                    LyDo = ""
                },

                new ChiTietDiemDanh()
                {
                    Id = 2,
                    DiemDanhId = 1,
                    SinhVienId = 2,
                    VangMat = VangMat.CoMat,
                    LyDo = ""
                },

                new ChiTietDiemDanh()
                {
                    Id = 3,
                    DiemDanhId = 1,
                    SinhVienId = 3,
                    VangMat = VangMat.VangMat,
                    LyDo = ""
                },

                new ChiTietDiemDanh()
                {
                    Id = 4,
                    DiemDanhId = 2,
                    SinhVienId = 1,
                    VangMat = VangMat.Muon,
                    LyDo = ""
                },

                new ChiTietDiemDanh()
                {
                    Id = 5,
                    DiemDanhId = 2,
                    SinhVienId = 2,
                    VangMat = VangMat.CoMat,
                    LyDo = ""
                },

                new ChiTietDiemDanh()
                {
                    Id = 6,
                    DiemDanhId = 2,
                    SinhVienId = 3,
                    VangMat = VangMat.CoMat,
                    LyDo = ""
                }
            );
        }
    }
}
