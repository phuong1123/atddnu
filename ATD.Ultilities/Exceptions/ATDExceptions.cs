﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ATD.Ultilities.Exceptions
{
    public class ATDExceptions : Exception
    {
        public ATDExceptions() 
        {
        }

        public ATDExceptions(string message)
            : base(message)
        {

        }

        public ATDExceptions(string message, Exception inner)
            : base(message, inner)
        {

        }
    }
}
