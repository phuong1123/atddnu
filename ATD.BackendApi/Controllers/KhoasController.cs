﻿using ATD.Application.Catalog.Khoas;
using ATD.BackendApi.Models;
using ATD.ViewModels.Catalog.Khoas;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Diagnostics;
using System.Threading.Tasks;

namespace ATD.BackendApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class KhoasController : ControllerBase
    {
        private readonly IKhoaService _khoaService;
        public KhoasController(IKhoaService khoaService)
        {
            _khoaService = khoaService;
        }


        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var khoa = await _khoaService.GetAll();
            return Ok(khoa);
        }

        [HttpGet("public-paging")]
        public async Task<IActionResult> GetAllPaging([FromQuery] GetKhoaPagingRequest request)
        {
            var khoa = await _khoaService.GetAllByKhoaId(request);
            return Ok(khoa);
        }

        [HttpGet("{khoaId}")]
        public async Task<IActionResult> GetById(int khoaId)
        {
            var khoa = await _khoaService.GetById(khoaId);
            if (khoa == null) return BadRequest("Không tim thấy khoa này!");
            return Ok(khoa);
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromForm] KhoaCreateRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var khoaId = await _khoaService.Create(request);
            if (khoaId == 0) return BadRequest();

            var khoa = await _khoaService.GetById(khoaId);

            //return Ok(); // trả về 200
            return CreatedAtAction(nameof(GetById), new { id = khoaId }, khoa); // trả về 201
        }

        [HttpPut]
        public async Task<IActionResult> Update([FromForm] KhoaUpdateRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var result = await _khoaService.Update(request);
            if (result == 0) return BadRequest();
            return Ok();
        }

        [HttpDelete("{khoaId}")]
        public async Task<IActionResult> Delete(int khoaId)
        {
            var result = await _khoaService.Delete(khoaId);
            if (result == 0) return BadRequest();
            return Ok();
        }
    }
}
