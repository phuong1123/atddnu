﻿using ATD.Application.Catalog.NamHocs;
using ATD.ViewModels.Catalog.NamHocs;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ATD.BackendApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NamHocsController : ControllerBase
    {
        private readonly INamHocService _manageNamHocService;
        public NamHocsController(INamHocService manageNamHocService)
        {
            _manageNamHocService = manageNamHocService;
        }


        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var namHoc = await _manageNamHocService.GetAll();
            return Ok(namHoc);
        }

        [HttpGet("public-paging")]
        public async Task<IActionResult> GetAllPaging([FromQuery] GetNamHocPagingRequest request)
        {
            var namHoc = await _manageNamHocService.GetAllByNamHocId(request);
            return Ok(namHoc);
        }

        [HttpGet("{namHocId}")]
        public async Task<IActionResult> GetById(int namHocId)
        {
            var namHoc = await _manageNamHocService.GetById(namHocId);
            if (namHoc == null) return BadRequest("Không tim thấy khoa này!");
            return Ok(namHoc);
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromForm] NamHocCreateRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var namHocId = await _manageNamHocService.Create(request);
            if (namHocId == 0) return BadRequest();

            var namHoc = await _manageNamHocService.GetById(namHocId);

            //return Ok(); // trả về 200
            return CreatedAtAction(nameof(GetById), new { id = namHocId }, namHoc); // trả về 201
        }

        [HttpPut]
        public async Task<IActionResult> Update([FromForm] NamHocUpdateRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var result = await _manageNamHocService.Update(request);
            if (result == 0) return BadRequest();
            return Ok();
        }

        [HttpDelete("{namHocId}")]
        public async Task<IActionResult> Delete(int namHocId)
        {
            var result = await _manageNamHocService.Delete(namHocId);
            if (result == 0) return BadRequest();
            return Ok();
        }
    }
}
