﻿using ATD.Application.Dtos;
using ATD.Data.Entities;
using ATD.Data.EntitiesFramework;
using ATD.Ultilities.Exceptions;
using ATD.ViewModels.Catalog.NamHocs;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace ATD.Application.Catalog.NamHocs
{
    public class NamHocService : INamHocService
    {
        private readonly AtdDbContext _context;
        public NamHocService (AtdDbContext context)
        {
            _context = context;
        }

        public async Task<int> Create(NamHocCreateRequest request)
        {
            var namHoc = new NamHoc()
            {
                TenNamHoc = request.TenNamHoc,
                GhiChu = request.GhiChu
            };

            _context.NamHocs.Add(namHoc);
            await _context.SaveChangesAsync();
            return namHoc.Id;
        }

        public async Task<int> Delete(int namHocId)
        {
            var namHoc = await _context.NamHocs.FindAsync(namHocId);
            if (namHoc == null) throw new ATDExceptions($"Không tim thấy năm học {namHocId} này!");
            _context.NamHocs.Remove(namHoc);
            return await _context.SaveChangesAsync();
        }

        public async Task<List<NamHocViewModel>> GetAll()
        {
            var query = from p in _context.NamHocs
                        select p;

            int totalRow = await query.CountAsync();

            var data = await query.Select(x => new NamHocViewModel()
            {
                Id = x.Id,
                TenNamHoc = x.TenNamHoc,
                GhiChu = x.GhiChu
            }).ToListAsync();

            return data;
        }

        public async Task<PagedResult<NamHocViewModel>> GetAllByNamHocId(GetNamHocPagingRequest request)
        {
            //1.Select join
            var query = from p in _context.NamHocs
                        select p;

            //2.filter

            if (request.NamHocId.HasValue && request.NamHocId.Value > 0)
            {
                query = query.Where(p => p.Id == request.NamHocId);
            }

            //3. paging
            int totalRow = await query.CountAsync();

            var data = await query.Skip((request.PageIndex - 1) * request.PageSize)
                .Take(request.PageSize)
                .Select(x => new NamHocViewModel()
                {
                    Id = x.Id,
                    TenNamHoc = x.TenNamHoc,
                    GhiChu = x.GhiChu
                }).ToListAsync();

            //4.select and prijection
            var pageResult = new PagedResult<NamHocViewModel>()
            {
                TotalRecord = totalRow,
                Items = data
            };

            return pageResult;
        }

        public async Task<PagedResult<NamHocViewModel>> GetAllPaging(GetManageNamHocPagingRequest request)
        {
            //1.Select join
            var query = from p in _context.NamHocs
                        select p;

            //2.filter
            if (!string.IsNullOrEmpty(request.keyword))
                query = query.Where(p => p.TenNamHoc.Contains(request.keyword));

            if (request.NamHocIds.Count > 0)
            {
                query = query.Where(p => request.NamHocIds.Contains(p.Id));
            }

            //3. paging
            int totalRow = await query.CountAsync();

            var data = await query.Skip((request.PageIndex - 1) * request.PageSize)
                .Take(request.PageSize)
                .Select(x => new NamHocViewModel()
                {
                    Id = x.Id,
                    TenNamHoc = x.TenNamHoc,
                    GhiChu = x.GhiChu
                }).ToListAsync();

            //4.select and prijection
            var pageResult = new PagedResult<NamHocViewModel>()
            {
                TotalRecord = totalRow,
                Items = data
            };

            return pageResult;
        }

        public async Task<NamHocViewModel> GetById(int namHocId)
        {
            var namHoc = await _context.NamHocs.FindAsync(namHocId);
            var namHocViewModel = new NamHocViewModel()
            {
                Id = namHoc.Id,
                TenNamHoc = namHoc.TenNamHoc,
                GhiChu = namHoc.GhiChu                
            };

            return namHocViewModel;
        }

        public async Task<int> Update(NamHocUpdateRequest request)
        {
            var namHoc = await _context.NamHocs.FindAsync(request.Id);
            if (namHoc == null) throw new ATDExceptions($"Không tìm thấy khoa {request.Id} này");

            namHoc.TenNamHoc = request.TenNamHoc;
            namHoc.GhiChu = request.GhiChu;

            return await _context.SaveChangesAsync();
        }
    }
}
