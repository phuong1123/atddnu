﻿using ATD.Application.Dtos;
using ATD.ViewModels.Catalog.NamHocs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ATD.Application.Catalog.NamHocs
{
    public interface INamHocService
    {
        Task<int> Create(NamHocCreateRequest request);

        Task<int> Update(NamHocUpdateRequest request);

        Task<int> Delete(int namHocId);

        Task<NamHocViewModel> GetById(int namHocId);

        Task<List<NamHocViewModel>> GetAll();

        Task<PagedResult<NamHocViewModel>> GetAllByNamHocId(GetNamHocPagingRequest request);

        Task<PagedResult<NamHocViewModel>> GetAllPaging(GetManageNamHocPagingRequest request);
    }
}
