﻿    using ATD.Application.Dtos;
using ATD.Data.Entities;
using ATD.Data.EntitiesFramework;
using ATD.ViewModels.Catalog.Khoas;
using System;
using ATD.Ultilities.Exceptions;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace ATD.Application.Catalog.Khoas
{
    public class KhoaService : IKhoaService
    {
        private readonly AtdDbContext _context;
        public KhoaService(AtdDbContext context)
        {
            _context = context;
        }

        public async Task<List<KhoaViewModel>> GetAll()
        {
            var query = from p in _context.Khoas
                        select p;

            int totalRow = await query.CountAsync();

            var data = await query.Select(x => new KhoaViewModel()
            {
                Id = x.Id,
                TenKhoa = x.TenKhoa,
                Sdt = x.Sdt,
                Email = x.Email,
                MoTa = x.MoTa
            }).ToListAsync();

            return data;
        }

        public async Task<PagedResult<KhoaViewModel>> GetAllByKhoaId(GetKhoaPagingRequest request)
        {
            //1.Select join
            var query = from p in _context.Khoas
                        select p;

            //2.filter

            if (request.KhoaId.HasValue && request.KhoaId.Value > 0)
            {
                query = query.Where(p => p.Id == request.KhoaId);
            }

            //3. paging
            int totalRow = await query.CountAsync();

            var data = await query.Skip((request.PageIndex - 1) * request.PageSize)
                .Take(request.PageSize)
                .Select(x => new KhoaViewModel()
                {
                    Id = x.Id,
                    TenKhoa = x.TenKhoa,
                    Sdt = x.Sdt,
                    Email = x.Email,
                    MoTa = x.MoTa
                }).ToListAsync();

            //4.select and prijection
            var pageResult = new PagedResult<KhoaViewModel>()
            {
                TotalRecord = totalRow,
                Items = data
            };

            return pageResult;
        }

        public async Task<int> Create(KhoaCreateRequest request)
        {
            var khoa = new Khoa()
            {
                TenKhoa = request.TenKhoa,
                Sdt = request.Sdt,
                Email = request.Email,
                MoTa = request.MoTa
            };

            _context.Khoas.Add(khoa);
            await _context.SaveChangesAsync();
            return khoa.Id;
        }

        public async Task<int> Delete(int khoaId)
        {
            var khoa = await _context.Khoas.FindAsync(khoaId);
            if (khoa == null) throw new ATDExceptions($"Không tim thấy khoa {khoaId} này!");
            _context.Khoas.Remove(khoa);
            return await _context.SaveChangesAsync();
        }


        public async Task<PagedResult<KhoaViewModel>> GetAllPaging(GetManageKhoaPagingRequest request)
        {
            //1.Select join
            var query = from p in _context.Khoas
                        select p;

            //2.filter
            if (!string.IsNullOrEmpty(request.keyword))
                query = query.Where(p => p.TenKhoa.Contains(request.keyword));

            if (request.KhoaIds.Count > 0)
            {
                query = query.Where(p => request.KhoaIds.Contains(p.Id));
            }

            //3. paging
            int totalRow = await query.CountAsync();

            var data = await query.Skip((request.PageIndex - 1) * request.PageSize)
                .Take(request.PageSize)
                .Select(x => new KhoaViewModel()
                {
                    Id = x.Id,
                    TenKhoa = x.TenKhoa,
                    Sdt = x.Sdt,
                    Email = x.Email,
                    MoTa = x.MoTa
                }).ToListAsync();

            //4.select and prijection
            var pageResult = new PagedResult<KhoaViewModel>()
            {
                TotalRecord = totalRow,
                Items = data
            };

            return pageResult;
        }

        public async Task<KhoaViewModel> GetById(int khoaId)
        {
            var khoa = await _context.Khoas.FindAsync(khoaId);
            var khoaViewModel = new KhoaViewModel()
            {
                Id = khoa.Id,
                TenKhoa = khoa.TenKhoa,
                Sdt = khoa.Sdt,
                Email = khoa.Email,
                MoTa = khoa.MoTa
            };

            return khoaViewModel;
        }

        public async Task<int> Update(KhoaUpdateRequest request)
        {
            var khoa = await _context.Khoas.FindAsync(request.Id);
            if (khoa == null) throw new ATDExceptions($"Không tìm thấy khoa {request.Id} này");

            khoa.TenKhoa = request.TenKhoa;
            khoa.Sdt = request.Sdt;
            khoa.Email = request.Email;
            khoa.MoTa = request.MoTa;

            return await _context.SaveChangesAsync();
        }
    }
}
