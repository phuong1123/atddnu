﻿using ATD.Application.Dtos;
using ATD.ViewModels.Catalog.Khoas;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ATD.Application.Catalog.Khoas
{
    public interface IKhoaService
    {
        Task<int> Create(KhoaCreateRequest request);

        Task<int> Update(KhoaUpdateRequest request);

        Task<int> Delete(int khoaId);

        Task<KhoaViewModel> GetById(int khoaId);

        Task<PagedResult<KhoaViewModel>> GetAllPaging(GetManageKhoaPagingRequest request);

        Task<PagedResult<KhoaViewModel>> GetAllByKhoaId(GetKhoaPagingRequest request);

        Task<List<KhoaViewModel>> GetAll();
    }
}
